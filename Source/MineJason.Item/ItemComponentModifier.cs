﻿using MineJason.SNbt.Values;

namespace MineJason.Item;

/// <summary>
/// Represents an item component modifier.
/// </summary>
/// <typeparam name="TValue">The type of the value.</typeparam>
public class ItemComponentModifier<TValue> where TValue : ISNbtWritable, ISNbtValue
{
    /// <summary>
    /// Initializes a new instance of the <see cref="ItemComponentModifier{TValue}"/> class.
    /// </summary>
    /// <param name="addition">If <see langword="true"/>, this modifier is an addition.</param>
    /// <param name="identifier">The identifier of the component.</param>
    /// <param name="value">The value.</param>
    public ItemComponentModifier(bool addition, ResourceLocation identifier, TValue value)
    {
        Addition = addition;
        Identifier = identifier;
        Value = value;
    }
    
    /// <summary>
    /// Gets or sets a value indicating whether this modifier adds the component or
    /// removes the specified component.
    /// </summary>
    public bool Addition { get; set; }
    
    /// <summary>
    /// Gets or sets the identifier of the component.
    /// </summary>
    public ResourceLocation Identifier { get; set; }
    
    /// <summary>
    /// Gets or sets the value of this instance.
    /// </summary>
    public TValue Value { get; set; }
}