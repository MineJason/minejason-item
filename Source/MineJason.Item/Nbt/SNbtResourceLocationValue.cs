﻿using JetBrains.Annotations;
using MineJason.SNbt;
using MineJason.SNbt.Values;

namespace MineJason.Item.Nbt;

/// <summary>
/// Represents a resource location value in string NBT.
/// </summary>
public readonly record struct SNbtResourceLocationValue : ISNbtValue, ISNbtWritable
{
    /// <summary>
    /// Initialises a new instance of the <see cref="SNbtResourceLocationValue"/> structure.
    /// </summary>
    /// <param name="value">The value.</param>
    public SNbtResourceLocationValue(ResourceLocation value)
    {
        Value = value;
    }
    
    [PublicAPI]
    public ResourceLocation Value { get; }
    
    public string ToSNbtString()
    {
        return SNbtStringValue.EscapeString(Value.ToString(), false);
    }

    public void WriteTo(SNbtWriter writer)
    {
        writer.WriteValue(Value.ToString());
    }
}