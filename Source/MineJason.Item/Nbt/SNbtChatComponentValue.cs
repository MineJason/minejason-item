﻿using System.Text.Json;
using MineJason.SNbt;
using MineJason.SNbt.Values;

namespace MineJason.Item.Nbt;

public sealed class SNbtChatComponentValue : ISNbtValue, ISNbtWritable
{
    public SNbtChatComponentValue(ChatComponent value)
    {
        Value = value;
    }
    
    public ChatComponent Value { get; set; }
    
    public string ToSNbtString()
    {
        return SNbtStringValue.EscapeString(JsonSerializer.Serialize(Value, typeof(ChatComponent)));
    }

    public void WriteTo(SNbtWriter writer)
    {
        writer.WriteValue(JsonSerializer.Serialize(Value), true);
    }
}