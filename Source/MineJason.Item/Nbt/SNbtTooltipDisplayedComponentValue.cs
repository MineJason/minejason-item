﻿using MineJason.SNbt;
using MineJason.SNbt.Values;

namespace MineJason.Item.Nbt;

public readonly record struct SNbtTooltipDisplayedComponentValue : ISNbtValue, ISNbtWritable
{
    /// <summary>
    /// Initialises a new instance of the <see cref="SNbtTooltipDisplayedComponentValue"/> structure.
    /// </summary>
    /// <param name="showInTooltip">If <see langword="true"/>, information about this component is shown in the tooltip.</param>
    public SNbtTooltipDisplayedComponentValue(bool showInTooltip)
    {
        ShowInTooltip = showInTooltip;
    }
    
    public bool ShowInTooltip { get; }
    
    public string ToSNbtString()
    {
        return ShowInTooltip ? "1b" : "0b";
    }

    public void WriteTo(SNbtWriter writer)
    {
        writer.WriteValue((ISNbtWritable)new SNbtByteValue(ShowInTooltip));
    }
}