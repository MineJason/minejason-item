﻿using MineJason.SNbt;
using MineJason.SNbt.Values;

namespace MineJason.Item.Nbt;

public sealed class SNbtEmptyComponentValue : ISNbtValue, ISNbtWritable
{
    public string ToSNbtString()
    {
        return "{}";
    }

    public void WriteTo(SNbtWriter writer)
    {
        writer.WriteBeginCompound();
        writer.WriteEndCompound();
    }
}