﻿using MineJason.Colors;
using MineJason.Item.Nbt;
using MineJason.SNbt.Values;

namespace MineJason.Item.Components;

// PURPOSE: Deal with item components not commonly shared, and not many enough to use a single
// partial.

public static partial class ItemComponent
{
    public static ItemComponentModifier<SNbtIntValue> MapColor(int color)
    {
        return new ItemComponentModifier<SNbtIntValue>(true,
            ItemComponentTypes.MapColor,
            color);
    }
    
    public static ItemComponentModifier<SNbtIntValue> MapId(int id)
    {
        return new ItemComponentModifier<SNbtIntValue>(true,
            ItemComponentTypes.MapId,
            id);
    }
    
    public static ItemComponentModifier<SNbtResourceLocationValue> Instrument(ResourceLocation instrument)
    {
        return new ItemComponentModifier<SNbtResourceLocationValue>(true,
            ItemComponentTypes.Instrument,
            new SNbtResourceLocationValue(instrument));
    }
    
    public static ItemComponentModifier<SNbtResourceLocationValue> NoteBlockSound(ResourceLocation sound)
    {
        return new ItemComponentModifier<SNbtResourceLocationValue>(true,
            ItemComponentTypes.NoteBlockSound,
            new SNbtResourceLocationValue(sound));
    }
    
    public static ItemComponentModifier<SNbtIntValue> OminousBottleAmplifier(int amplifier)
    {
        return new ItemComponentModifier<SNbtIntValue>(true, ItemComponentTypes.OminousBottleAmplifier, amplifier);
    }
}