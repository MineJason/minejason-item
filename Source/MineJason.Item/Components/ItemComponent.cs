﻿using JetBrains.Annotations;
using MineJason.Item.Nbt;

namespace MineJason.Item.Components;

/// <summary>
/// Provides methods to assist with the creation of item component modifier.
/// </summary>
[PublicAPI]
public static partial class ItemComponent
{
    /// <summary>
    /// Creates an item component with an empty value.
    /// </summary>
    /// <param name="componentType">The type to create.</param>
    /// <returns>The created component.</returns>
    private static ItemComponentModifier<SNbtEmptyComponentValue> InternalEmpty(ResourceLocation componentType)
    {
        return new ItemComponentModifier<SNbtEmptyComponentValue>(true, componentType, new SNbtEmptyComponentValue());
    }

    /// <summary>
    /// Creates an item component that have tooltip displaying configuration.
    /// </summary>
    /// <param name="componentType">The component type.</param>
    /// <param name="showInTooltip">If <see langword="true"/>, information about the component is shown in the tooltip.</param>
    /// <returns>The create component.</returns>
    private static ItemComponentModifier<SNbtTooltipDisplayedComponentValue> InternalTooltipDisplayed(
        ResourceLocation componentType,
        bool showInTooltip)
    {
        return new ItemComponentModifier<SNbtTooltipDisplayedComponentValue>(true,
            componentType,
            new SNbtTooltipDisplayedComponentValue(showInTooltip));
    }
}