﻿using System.ComponentModel.DataAnnotations;
using MineJason.Item.Nbt;
using MineJason.SNbt.Values;

namespace MineJason.Item.Components;

// PURPOSE: This partial deals with durability related components.

public static partial class ItemComponent
{
    /// <summary>
    /// Creates an item component that specifies the consumed durability of an item stack.
    /// </summary>
    /// <param name="damage">The consumed durability value. Must be positive.</param>
    /// <returns>The created component.</returns>
    /// <exception cref="ArgumentOutOfRangeException">The specified <paramref name="damage"/> is negative.</exception>
    public static ItemComponentModifier<SNbtIntValue> Damage([Range(0, int.MaxValue)] int damage)
    {
        if (damage < 0)
        {
            throw new ArgumentOutOfRangeException(nameof(damage), "Damage value must be positive.");
        }

        return new ItemComponentModifier<SNbtIntValue>(true, ItemComponentTypes.Damage, damage);
    }
    
    /// <summary>
    /// Creates an item component that specifies the maximum durability of an item stack.
    /// </summary>
    /// <param name="maxDamage">The maximum damage. Must be positive.</param>
    /// <returns>The created component.</returns>
    /// <exception cref="ArgumentOutOfRangeException">The specified <paramref name="maxDamage"/> is negative.</exception>
    public static ItemComponentModifier<SNbtIntValue> MaxDamage([Range(0, int.MaxValue)] int maxDamage)
    {
        if (maxDamage < 0)
        {
            throw new ArgumentOutOfRangeException(nameof(maxDamage), "Maximum damage value must be positive.");
        }

        return new ItemComponentModifier<SNbtIntValue>(true, ItemComponentTypes.MaxDamage, maxDamage);
    }
    
    /// <summary>
    /// Creates an item component that, when present, prevents the item stack being damaged.
    /// </summary>
    /// <param name="showInTooltip">If <see langword="true"/>, show "Unbreakable" in the tooltip.</param>
    /// <returns>The created component.</returns>
    public static ItemComponentModifier<SNbtTooltipDisplayedComponentValue> Unbreakable(bool showInTooltip = true)
    {
        return InternalTooltipDisplayed(ItemComponentTypes.Unbreakable, showInTooltip);
    }
}