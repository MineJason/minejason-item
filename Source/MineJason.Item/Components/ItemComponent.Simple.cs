﻿using System.ComponentModel.DataAnnotations;
using MineJason.Data;
using MineJason.Item.Nbt;
using MineJason.SNbt.Values;

namespace MineJason.Item.Components;

// This partial file deals with components shared with all items, except for enchantments,
// which is dealt with by the Enchantments partial file.

public static partial class ItemComponent
{
    public static ItemComponentModifier<SNbtChatComponentValue> CustomName(ChatComponent name)
    {
        return new ItemComponentModifier<SNbtChatComponentValue>(true, ItemComponentTypes.CustomName, 
            new SNbtChatComponentValue(name));
    }

    /// <summary>
    /// Creates an item component that, when present, prevents an item entity holding the item stack
    /// from being burned in fire and lava.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Items such as Netherite bears this default behaviour.
    /// </para>
    /// </remarks>
    /// <returns>The created component.</returns>
    public static ItemComponentModifier<SNbtEmptyComponentValue> FireResistant()
    {
        return new ItemComponentModifier<SNbtEmptyComponentValue>(true, ItemComponentTypes.FireResistant, new SNbtEmptyComponentValue());
    }

    /// <summary>
    /// Creates an item component that, when present, prevents certain supplementary information from being
    /// displayed in the tooltip for the item stack.
    /// </summary>
    /// <returns>The created component.</returns>
    public static ItemComponentModifier<SNbtEmptyComponentValue> HideAdditionalTooltip()
    {
        return InternalEmpty(ItemComponentTypes.HideAdditionalTooltip);
    }

    /// <summary>
    /// Creates an item component that, when present, prevents tooltip being displayed for the item stack.
    /// </summary>
    /// <returns>The created component.</returns>
    public static ItemComponentModifier<SNbtEmptyComponentValue> HideTooltip()
    {
        return InternalEmpty(ItemComponentTypes.HideTooltip);
    }
    
    /// <summary>
    /// Creates an item component that, when present, specifies the name of an item displayed when
    /// <see cref="ItemComponentTypes.CustomName"/> is not present.
    /// </summary>
    /// <param name="name">The name as a chat component.</param>
    /// <returns>The created component.</returns>
    public static ItemComponentModifier<SNbtChatComponentValue> ItemName(ChatComponent name)
    {
        return new ItemComponentModifier<SNbtChatComponentValue>(true, ItemComponentTypes.ItemName, 
            new SNbtChatComponentValue(name));
    }

    /// <summary>
    /// Creates an item component applied to a block entity item that, when present, locks the placed container
    /// and requires the item with the specified name (whether formatted or not) to open the container.
    /// </summary>
    /// <param name="key">The item name to open the container.</param>
    /// <returns>The created component.</returns>
    public static ItemComponentModifier<SNbtStringValue> Lock(string key)
    {
        return new ItemComponentModifier<SNbtStringValue>(true, ItemComponentTypes.Lock, 
            new SNbtStringValue(key));
    }

    /// <summary>
    /// Creates an item component that specified the maximum amount of items that the item stack can hold.
    /// </summary>
    /// <param name="stackSize">The maximum size. Must be between <c>1</c> and <c>99</c> (inclusive).</param>
    /// <returns></returns>
    /// <exception cref="ArgumentOutOfRangeException">The specified <paramref name="stackSize"/> is either smaller than <c>1</c> or bigger than <c>99</c>.</exception>
    public static ItemComponentModifier<SNbtIntValue> MaxStackSize([Range(1, 99)] int stackSize)
    {
        if (stackSize is < 1 or > 99)
        {
            throw new ArgumentOutOfRangeException(nameof(stackSize), "Maximum stack size must be between 0 and 99.");
        }

        return new ItemComponentModifier<SNbtIntValue>(true, ItemComponentTypes.MaxStackSize, stackSize);
    }

    /// <summary>
    /// Creates an item component that specifies the rarity of the item stack.
    /// </summary>
    /// <param name="rarity">The rarity. Must be one of the predifined values of <see cref="ItemRarity"/>.</param>
    /// <returns>The created component.</returns>
    /// <exception cref="ArgumentException">The rarity is not one of the predefined values of <see cref="ItemRarity"/>.</exception>
    public static ItemComponentModifier<SNbtStringValue> Rarity(ItemRarity rarity)
    {
        var valueStr = rarity switch
        {
            ItemRarity.Common => "common",
            ItemRarity.Uncommon => "uncommon",
            ItemRarity.Rare => "rare",
            ItemRarity.Epic => "epic",
            _ => throw new ArgumentException("Unrecognized rarity", nameof(rarity))
        };

        return new ItemComponentModifier<SNbtStringValue>(true, ItemComponentTypes.Rarity, valueStr);
    }

    /// <summary>
    /// Creates an item component that, when specified, additionally requires the specified amount of levels to
    /// rename or modify in an anvil.
    /// </summary>
    /// <remarks>
    /// <para>
    /// This component is used by anvils to implement the "prior work penalty" system. This works by, upon each modification
    /// of an item stack, increasing the value of this component depending on the specific action done in anvil. Renaming the item
    /// does not increase prior work penalty.
    /// </para>
    /// <para>
    /// If the overall cost to modify or rename an item (that is, the base cost required to modify or rename an item,
    /// plus the repair cost altogether) is more than <c>39</c>, the item will no longer be able to be renamed nor be modified
    /// in an Anvil for players in <see cref="GameMode.Survival"/> and <see cref="GameMode.Adventure"/> modes. This does not
    /// apply to <see cref="GameMode.Creative"/> mode.
    /// </para>
    /// </remarks>
    /// <param name="cost">The cost.</param>
    /// <returns>The created component.</returns>
    /// <seealso href="https://minecraft.wiki/w/Anvil">Anvil on Minecraft Wiki</seealso>
    /// <seealso href="https://minecraft.wiki/w/Anvil_mechanics">Anvil mechanics on Minecraft Wiki</seealso>
    public static ItemComponentModifier<SNbtIntValue> RepairCost(int cost)
    {
        return new ItemComponentModifier<SNbtIntValue>(true, ItemComponentTypes.RepairCost, cost);
    }
}