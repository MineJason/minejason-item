﻿using JetBrains.Annotations;

namespace MineJason.Item.Components;

/// <summary>
/// Defines the identifiers for item component types.
/// </summary>
[PublicAPI]
public static class ItemComponentTypes
{
    public static readonly ResourceLocation CustomName = new("minecraft", "custom_name");
    public static readonly ResourceLocation Damage = new("minecraft", "damage");
    public static readonly ResourceLocation EnchantmentGlintOverride = new("minecraft", "enchantment_glint_override");
    public static readonly ResourceLocation FireResistant = new("minecraft", "fire_resistant");
    public static readonly ResourceLocation HideAdditionalTooltip = new("minecraft", "hide_additional_tooltip");
    public static readonly ResourceLocation HideTooltip = new("minecraft", "hide_tooltip");
    public static readonly ResourceLocation Instrument = new("minecraft", "instrument");
    public static readonly ResourceLocation ItemName = new("minecraft", "item_name");
    public static readonly ResourceLocation Lock = new("minecraft", "lock");
    public static readonly ResourceLocation MapColor = new("minecraft", "map_color");
    public static readonly ResourceLocation MapDecorations = new("minecraft", "map_decorations");
    public static readonly ResourceLocation MapId = new("minecraft", "map_id");
    public static readonly ResourceLocation MaxDamage = new("minecraft", "max_damage");
    public static readonly ResourceLocation MaxStackSize = new("minecraft", "max_stack_size");
    public static readonly ResourceLocation NoteBlockSound = new("minecraft", "note_block_sound");
    public static readonly ResourceLocation OminousBottleAmplifier = new("minecraft", "ominous_bottle_amplifier");
    public static readonly ResourceLocation Rarity = new("minecraft", "rarity");
    public static readonly ResourceLocation RepairCost = new("minecraft", "repair_cost");
    public static readonly ResourceLocation Unbreakable = new("minecraft", "unbreakable");
}