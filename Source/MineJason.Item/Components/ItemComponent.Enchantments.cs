﻿using MineJason.SNbt.Values;

namespace MineJason.Item.Components;

// This partial file deals with Enchantment related components.

public static partial class ItemComponent
{
    /// <summary>
    /// Creates an item component that override the display of the enchantment glint. To preserve the default glint behaviour,
    /// omit this component.
    /// </summary>
    /// <param name="glint">If <see langword="true"/>, the enchantment glint will display even if there are no enchantments; otherwise, the enchantment glint will be hidden even if there are enchantments.</param>
    /// <returns>The created component modifier.</returns>
    public static ItemComponentModifier<SNbtByteValue> EnchantmentGlintOverride(bool glint)
    {
        return new ItemComponentModifier<SNbtByteValue>(true, ItemComponentTypes.EnchantmentGlintOverride,
            new SNbtByteValue(glint));
    }
}