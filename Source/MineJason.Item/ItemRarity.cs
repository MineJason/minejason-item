﻿namespace MineJason.Item;

public enum ItemRarity
{
    Common,
    Uncommon,
    Rare,
    Epic
}