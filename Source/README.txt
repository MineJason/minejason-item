This directory contains the source files of the MineJason.Items extension
module.

See the main README.md file for building these source codes.

You should be able to edit these files by opening the `MineJason.Item.sln` file
in your favourite IDE or editor. This can be Visual Studio, VS Code, Rider, or
an editor with OmniSharp extension.

Make sure your IDE/editor respects `.editorconfig`.