# MineJason.Item

This is the MineJason Items Extension Module, provides support for:

- `ItemStack` representation model
- Representation models for item components
- Display Item hover event

## Usage

WIP

## Building

This project should be build in the same manner as all other .NET projects. Make sure you have .NET 8 SDK, navigate to the directory, and enter `dotnet build`.
